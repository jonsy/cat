//Powered By if, Since 2014 - 2020

package com.zs.pig.cms.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.cms.api.model.Nav;


/**
 * 
 * @author zsCat 2016-10-13 14:37:49
 * @Email: 951449465@qq.coms
 * @version 4.0v
 *	我的官网
 */
public interface NavMapper extends Mapper<Nav>{

}
