//Powered By if, Since 2014 - 2020

package com.zs.pig.[model].api.model;
import java.util.Date;
import java.math.BigDecimal;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;


/**
 * 
 * @author [author] [date]
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	[description]
 */
@SuppressWarnings({ "unused"})
@Table(name="[tablename]")
public class [entityClass] extends BaseEntity {

	private static final long serialVersionUID = 1L;

  		 [entitybuffer]

}
